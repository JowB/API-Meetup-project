<?php

class Login {

    function __construct(){
        //  Clé d'encryptage (secrète)
        $key = "sCcLcjMfAkFJM3jB";

        //  Données (playload)
        $data = [
            "user_id" => 1,
            "exp" => time() + 10*24*60*60,
            "iat" => time(),
        ];

        //  Création du token
        $token = JWT::encode($data, $key);

        //  Retourne le JSON
        echo json_encode(['token' => $token]);
    }
}
