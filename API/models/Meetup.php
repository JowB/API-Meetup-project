<?php

require_once 'models/DB.php';

class Meetup extends DB {

    //  Function pour afficher les meetup
    public function getAllMeetup(){
        $request = $this->db->query('SELECT * FROM meetup');
        $fetchAll = $request->fetchAll(PDO::FETCH_ASSOC);
        return $fetchAll;
    }
    
    //  Function pour afficher un seul meetup en fonction de l'id
    public function getMeetupById($id){
        $request = $this->db->prepare('SELECT * FROM meetup WHERE id = :id');
        $request->execute(['id' => $id]);
        $fetch = $request->fetch();
        return $fetch;
    }

    //  Function pour supprimer un meetup
    public function deleteMeetupById($id){
        $request = $this->db->prepare('DELETE FROM meetup WHERE id = :id');
        $request->execute(['id' => $id]);
    }

    //  Function pour créer un meetup
    public function createMeetup($title, $date, $description, $location , $participant, $intervenant) {
        $request = $this->db->prepare('INSERT INTO meetup VALUES(NULL, :title, :date, :description, :location, :participant, :intervenant)');
        $request->execute(['title' => $title,
            'date' => $date,
            'description' => $description,
            'location' => $location,
            'participant' => $participant,
            'intervenant' => $intervenant]);

        return [
            'id' => $this->db->lastInsertId(),
            'title' => $title,
            'date' => $date,
            'description' => $description,
            'location' => $location,
            'participant' => $participant,
            'intervenant' => $intervenant
        ];
    }

    //  Function pour modifier un meetup
    public function updateMeetupById($id, $title, $date, $description, $location, $participant, $intervenant){
        $request = $this->db->prepare('UPDATE meetup SET title=:title, date=:date, description=:description, location=:location, participant=:participant, intervenant=:intervenant WHERE id=:id');
        $request->execute(['id' => $id,
            'title' => $title,
            'date' => $date,
            'description' => $description,
            'location' => $location,
            'participant' => $participant,
            'intervenant' => $intervenant]);
    }
}

?>