<?php

class DB {
    public $db;

    function __construct(){
        $user = 'root';
        $password = 'root';
        $host = 'localhost';
        $database = 'APIMeetUp';
 
        $this->db = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $password);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
   
 }

?>