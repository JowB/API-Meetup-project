<?php

require_once __DIR__.'/../models/Meetup.php';
require_once __DIR__.'/Controller.php';

class MeetupController extends Controller {

    public function getAll(){
        $data = new Meetup;
        $datas = $data->getAllMeetup();

        return json_encode($datas);
    }

    public function addMeetup(){
        $title = $_POST['title'];
        $date = $_POST['date'];
        $description = $_POST['description'];
        $location = $_POST['location'];
        $participant = $_POST['participant'];
        $intervenant = $_POST['intervenant'];

        $data = new Meetup;
        $datas = $data->createMeetup($title, $date, $description, $location , $participant, $intervenant);

        return json_encode($datas);
    }

    public function getMeetupWithId($id){
        $data = new Meetup;
        $datas = $data->getMeetupById($id);

        return json_encode($datas);
    }

    public function deleteById($id){
        $data = new Meetup;
        $datas = $data->deleteMeetupById($id);

        return json_encode($datas);
    }

    public function updateById($id){
        $put = $this->getHttpData();

        $id = $put['id'];
        $title = $put['title'];
        $date = $put['date'];
        $description = $put['description'];
        $location = $put['location'];
        $participant = $put['participant'];
        $intervenant = $put['intervenant'];

        
        $data = new Meetup;
        $datas = $data->updateMeetupById($id, $title, $date, $description, $location , $participant, $intervenant);

        return json_encode($datas);
    }
}