var db = {};

//                  Afficher les Meetup
$(document).ready(getFullMeetup);

function getFullMeetup() {

    fetch('../API/meetups')
        .then(function (response) {
            return response.json();
        })
        .then(function (json) {
            db.meetups = json;
            json.forEach(function (json) {
                $('.container-meetup').append(`<div class="meetup" data-id="${json.id}"></div>`)
                $('.meetup:last').append(
                    `<div class="meetup-titre">${json.title}</div>`,
                    `<div class="meetup-description">${json.description}</div>`,
                    `<div class="meetup-date">${json.date}</div>`,
                    `<button type="button" class="btn btn-primary js-info" data-id="${json.id}">Plus d'infos</button>`,
                    `<button type="button" class="btn btn-warning js-modif" data-id="${json.id}">Modifier</button>`,
                    `<button type="button" class="btn btn-danger js-suppr" data-id="${json.id}">Supprimer</button>`
                )
            })

        })

}


//                  Afficher un seul Meetup
$('body').on('click', '.js-info', getMeetup);

function getMeetup() {

    $.ajax({
        url: '../API/meetups/' + $(this).attr('data-id'),
        type: 'GET'
    }).done(function (data) {
        data = JSON.parse(data);
        $('.container-add').hide();
        $('.container-master').empty();
        $('.container-master').append(`<div class="container-meetup"></div>`);
        $('.container-meetup').append(`<div class="meetup" data-id="${data.id}"></div>`)
        $('.meetup:last').append(
            `<div class="meetup-titre">${data.title}</div>`,
            `<div class="meetup-description">${data.description}</div>`,
            `<div class="meetup-date">${data.date}</div>`,
            `<button type="button" class="btn btn-primary js-retour">Retour liste des Meet Up</button>`
        )
    });

}

//                  Retour page des Meetup
$('body').on('click', '.js-retour', getFullMeetup)

//                  Add Meetup
$('#add-meetup-button').on('click', addMeetup)

function addMeetup() {

    $.ajax({
        url: '../API/meetups',
        type: 'POST',
        data: {
            title: $('[name=title]').val(),
            date: $('[name=date]').val(),
            location: $('[name=location]').val(),
            description: $('[name=description]').val(),
            participant: $('[name=participant]').val(),
            intervenant: $('[name=intervenant').val()
        }
    }).done(function (data) {
        data = JSON.parse(data);
        $('.container-meetup').append(`<div class="meetup" data-id="${data.id}">`)
        $('.meetup:last').append(
            `<div class='meetup-titre'>${data.title}</div>`,
            `<div class='meetup-description'>${data.description}</div>`,
            `<div class='meetup-date'>${data.date}</div>`,

        )
    });

}

//                  Supprimer un Meetup
$(document).on('click', '.js-suppr', deleteMeetup)

function deleteMeetup() {

    var currentDiv = $(this).parent();
    
    $.ajax({
        url: '../API/meetups/' + $(this).parent().attr('data-id'),
        type: 'DELETE'
    }).done(function (data) {
        currentDiv.remove();
        
    })

}

//                  Modifier un Meetup (l'affiche avec des input)
$(document).on('click', '.js-modif', editMeetup)

function editMeetup() {

    $.ajax({
        url: '../API/meetups/' + $(this).attr('data-id'),
        type: 'GET'
    }).done(function (data) {
        data = JSON.parse(data);
        $('.container-add').hide();
        $('.container-center').empty();
        $('.container-center').append(`<h3 class="titre">Modification du Meetup</h3>`);
        $('.container-center').append(`<div class="container-meetup"></div>`);
        $('.container-meetup').append(`<div class="meetup" data-id="${data.id}"></div>`)
        $('.meetup:last').append(
            `Titre du Meetup<input type="text" class="meetup-titre" name="title" value="${data.title}">`,
            `Description du Meetup<textarea name="description" class="meetup-description">${data.description}</textarea>`,
            `Date du Meetup<input type="date" class="meetup-date" name="date" value="${data.date}">`,
            `<input type="hidden" name="id" value="${data.id}">`,
            `<button type="button" class="btn btn-primary js-modif-envoi" value="${data.id}">Modifier</button>`
        )
    });

}

//                  Modifier un Meetup (dernière étape: l'envoi à la BDD)
$(document).on('click', '.js-modif-envoi', updateMeetup)

function updateMeetup() {

    $.ajax({
        url: '../API/meetups/' + $(this).parent().attr('data-id'),
        type: 'PUT',
        data: {
            id: $('[name=id]').val(),
            title: $('[name=title]').val(),
            date: $('[name=date]').val(),
            location: $('[name=location]').val(),
            description: $('[name=description]').val(),
            participant: $('[name=participant]').val(),
            intervenant: $('[name=intervenant').val()
        }
    }).done(function(data){
        $('.container-meetup').empty();
        $('.titre').html("Liste des Meet Up");
        $('.container-add').show();
        getFullMeetup();
    })
}