API-Meetup-project


Réaliser un site d'évènements (meetup). Le projet à pour but la mise en place d'une API REST en PHP et son utilisation sur un site internet. Il se découpe donc comme suit :

    -> API REST PHP => Base de données
    -> Site internet full front JavaScript

Les appels à l'API depuis le site devront se réaliser en AJAX.


-> Afficher la liste des events sur la page 
-> Supprimer un event en AJAX
-> Formulaire d'ajout d'un event en AJAX (et ajout automatique de l'évènement HTML à la page)
-> Mise à jour de l'event
-> Page d'affichage d'un event
-> BDD pour les events
-> API permettant le CRUD sur les events